#include "../cptk.h"
#include <iostream>
using namespace cptk;
int main(){
    const int N = 8;
    int X[N] = {2, 7, 5, 4, 8, 2, 3, 6};
    int n;
    std::cin >> n;
    array<int> a(n);
    for(auto i = 0; i < n; ++i)
        a[i] = i*2 + 2;
    auto p = a[5];
    std::cout << a[0] << ' ' << p <<  '\n';
}